# Keyboard shortcuts for bash

## Navigation
| Combination | Description |
|---|---|
| Ctrl + a | Jump to the beginning of the line |
| Ctrl + e | Jump to the end of the line |
| Ctrl + f | Forward one character |
| Ctrl + b | Backward one character |
| Ctrl + xx | Jump to the beginning of the line. Press again to return to the previous position |
| Alt + f / ESC - f (MacOS) | Walk one word forward |
| Alt + b / ESC - b (MacOS) | Walk one word backward |

## Deletion
| Combination | Description | Alternative |
|---|---|---|
| Ctrl + w | Delete previous word | Alt/Meta + Backspace |
| Ctrl + u | Delete all characters from the cursor to the beginning of the line | |
| Ctrl + k | Delete all characters from the cursor to the end of the line | |

#### Sources
* http://ss64.com/bash/syntax-keyboard.html
* http://www.howtogeek.com/howto/ubuntu/keyboard-shortcuts-for-bash-command-shell-for-ubuntu-debian-suse-redhat-linux-etc/
* `man bash`
